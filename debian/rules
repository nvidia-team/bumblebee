#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

get_variant		 = $(word 1,$(subst _, ,$1))
get_constraint		 = $(1:$(call get_variant,$1)%=%)

nv_variant_Debian	+= tesla-535_[amd64_arm64_ppc64el]

nv_driver_Debian	+= nvidia-driver_[!i386_!armhf]
nv_driver_Debian	+= $(foreach v,$(nv_variant_Debian),nvidia-$(call get_variant,$v)-driver$(call get_constraint,$v))
nv_driver_Debian	+= nvidia-driver-any

nv_module_Debian	+= nvidia
nv_module_Debian	+= nvidia-current
nv_module_Debian	+= $(foreach v,$(nv_variant_Debian),nvidia-$(call get_variant,$v))

nv_version_Ubuntu	+= 560_[amd64_arm64]
nv_version_Ubuntu	+= 550_[amd64_arm64]
nv_version_Ubuntu	+= 550-server_[amd64_arm64]
nv_version_Ubuntu	+= 535_[amd64_arm64]
nv_version_Ubuntu	+= 535-server_[amd64_arm64]

nv_driver_Ubuntu	+= nvidia-driver-binary_[amd64_i386]
nv_driver_Ubuntu	+= $(foreach v,$(nv_version_Ubuntu),nvidia-driver-$(call get_variant,$v)$(call get_constraint,$v))

nv_module_Ubuntu	+= $(filter-out nvidia-driver-binary,$(foreach v,$(nv_driver_Ubuntu),$(call get_variant,$v)))


VENDOR	:= $(shell dpkg-vendor --derives-from Ubuntu && echo Ubuntu || echo Debian)

bb_depends_Debian	 =
bb_depends_Ubuntu	 = pciutils

bbnv_depends_Debian	 = glx-alternative-nvidia (>= 0.6.92)
bbnv_depends_Ubuntu	 =

bb_backend_Debian	 = primus-libs
bb_backend_Ubuntu	 = virtualgl | primus-libs

MAINTSCRIPTS_GENERATED		 = $(MAINTSCRIPTS_GENERATED.$(VENDOR))
MAINTSCRIPTS_GENERATED		+= debian/bumblebee.conf
MAINTSCRIPTS_GENERATED.Debian	+= debian/bumblebee-nvidia.postinst
MAINTSCRIPTS_GENERATED.Ubuntu	+= debian/bumblebee-nvidia.postinst
MAINTSCRIPTS_GENERATED.Ubuntu	+= debian/bumblebee-nvidia.postrm

DPKG_EXPORT_BUILDFLAGS = 1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
include /usr/share/dpkg/default.mk

export deb_systemdsystemunitdir = $(shell pkg-config --variable=systemdsystemunitdir systemd | sed s,^/,,)
export deb_udevdir = $(shell pkg-config --variable=udevdir udev | sed s,^/,,)

%:
	dh $@

override_dh_auto_configure: $(MAINTSCRIPTS_GENERATED)
ifeq ($(VENDOR),Ubuntu)
	dh_auto_configure -- \
		--with-udev-rules=/$(deb_udevdir)/rules.d \
		CONF_DRIVER_MODULE_NVIDIA=nvidia \
		CONF_LDPATH_NVIDIA=/usr/lib/nvidia-current:/usr/lib32/nvidia-current:/usr/lib/x86_64-linux-gnu:/usr/lib/i386-linux-gnu \
		CONF_MODPATH_NVIDIA=/usr/lib/nvidia-current/xorg,/usr/lib/xorg/modules \
		CONF_PRIMUS_LD_PATH=/usr/lib/x86_64-linux-gnu/primus:/usr/lib/i386-linux-gnu/primus \
		CONF_XORG_BINARY=/usr/lib/xorg/Xorg
else
	dh_auto_configure -- \
		--with-udev-rules=/$(deb_udevdir)/rules.d \
		CONF_DRIVER_MODULE_NVIDIA=nvidia \
		CONF_LDPATH_NVIDIA=/usr/lib/x86_64-linux-gnu/nvidia:/usr/lib/i386-linux-gnu/nvidia:/usr/lib/x86_64-linux-gnu:/usr/lib/i386-linux-gnu \
		CONF_MODPATH_NVIDIA=/usr/lib/nvidia,/usr/lib/xorg/modules \
		CONF_PRIMUS_LD_PATH=/usr/lib/x86_64-linux-gnu/primus:/usr/lib/i386-linux-gnu/primus \
		CONF_XORG_BINARY=/usr/lib/xorg/Xorg
endif

execute_after_dh_clean:
	$(RM) $(MAINTSCRIPTS_GENERATED)

override_dh_installinit:
	dh_installinit --name=bumblebeed

override_dh_bugfiles:
	dh_bugfiles -A

override_dh_gencontrol:
	dh_gencontrol -- \
		-V'bumblebee:Depends=$(bb_depends_$(VENDOR))' \
		-V'nvidia:Depends=$(bbnv_depends_$(VENDOR)), $(subst _, ,$(foreach d,$(nv_driver_$(VENDOR)),$(d) |))' \
		-V'bumblebee:backend=$(bb_backend_$(VENDOR))'

debian/%: debian/%.$(VENDOR)
	cp -a $< $@

debian/bumblebee.conf: debian/rules
	$(RM) $@
	echo '# This file is installed by bumblebee, do NOT edit!' >> $@
	echo '# to be used by kmod / module-init-tools, and installed in /etc/modprobe.d/' >> $@
	echo '# or equivalent' >> $@
	echo '' >> $@
	echo '# do not automatically load nouveau as it may prevent nvidia from loading' >> $@
	echo 'blacklist nouveau' >> $@
	echo '' >> $@
	echo '# do not automatically load nvidia as it'"'"'s unloaded anyway when bumblebeed' >> $@
	echo '# starts and may fail bumblebeed to disable the card in a race condition.' >> $@
	for module in $(nv_module_$(VENDOR)) ; do echo "blacklist $$module" >> $@ ; done
